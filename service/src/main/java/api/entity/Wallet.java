package api.entity;

import javax.persistence.*;

@Entity
@Table(name = "wallet")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="user_id")
    private Long userId;
    private Float PLN=0f;
    private Float USD=0f;
    private Float EUR=0f;
    private Float GBP=0f;
    private Float CAD=0f;

    public Wallet(){
    }

    public Wallet(Long userId, Float PLN, Float USD, Float EUR, Float GBP, Float CAD) {
        this.userId = userId;
        this.PLN = PLN;
        this.USD = USD;
        this.EUR = EUR;
        this.GBP = GBP;
        this.CAD = CAD;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Float getPLN() {
        return PLN;
    }

    public void setPLN(Float PLN) {
        this.PLN = PLN;
    }

    public Float getUSD() {
        return USD;
    }

    public void setUSD(Float USD) {
        this.USD = USD;
    }

    public Float getEUR() {
        return EUR;
    }

    public void setEUR(Float EUR) {
        this.EUR = EUR;
    }

    public Float getGBP() {
        return GBP;
    }

    public void setGBP(Float GBP) {
        this.GBP = GBP;
    }

    public Float getCAD() {
        return CAD;
    }

    public void setCAD(Float CAD) {
        this.CAD = CAD;
    }
}
