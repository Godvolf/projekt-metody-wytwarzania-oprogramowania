package api.repository;

import api.entity.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {

    @Modifying
    @Transactional
    @Query(value = "UPDATE wallet SET PLN=PLN + ?2 WHERE user_id=?1", nativeQuery = true)
    void transfer(Long userId, Float number);

    @Query(value = "SELECT PLN FROM wallet  WHERE user_id=?1", nativeQuery = true)
    Float numberOfPLN(Long userId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE wallet SET PLN=PLN - ?3, USD = USD + ?2  WHERE user_id=?1", nativeQuery = true)
    void exchangeUSD(Long userId, Float numberOfCurrency, Float numberOfPLN);

    @Modifying
    @Transactional
    @Query(value = "UPDATE wallet SET PLN=PLN - ?3, EUR = EUR + ?2  WHERE user_id=?1", nativeQuery = true)
    void exchangeEUR(Long userId, Float numberOfCurrency, Float numberOfPLN);

    @Modifying
    @Transactional
    @Query(value = "UPDATE wallet SET PLN=PLN - ?3, GBP = GBP + ?2  WHERE user_id=?1", nativeQuery = true)
    void exchangeGBP(Long userId, Float numberOfCurrency, Float numberOfPLN);

    @Modifying
    @Transactional
    @Query(value = "UPDATE wallet SET PLN=PLN - ?3, CAD = CAD + ?2  WHERE user_id=?1", nativeQuery = true)
    void exchangeCAD(Long userId, Float numberOfCurrency, Float numberOfPLN);

    Wallet findByUserId(Long user_id);
}