package api.repository;

import api.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    @Modifying
    @Transactional
    @Query(value = "UPDATE users SET password = ?1  WHERE username=?2", nativeQuery = true)
    void setPassword(String newPassword, String username);

    // @Query(value = "SELECT id FROM users WHERE username=?1", nativeQuery = true)
    // Long findId(String username);

}
