package api.repository;

import api.entity.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Long> {
    //ExchangeRate findByUsername(String username);

    List<ExchangeRate> findAll();
    List<ExchangeRate> findByBank(String bank);
    List<ExchangeRate> findByBankAndCurrency(String bank,String currency);

    // @Query(value = "SELECT id FROM users WHERE username=?1", nativeQuery = true)
    // Long findId(String username);

}