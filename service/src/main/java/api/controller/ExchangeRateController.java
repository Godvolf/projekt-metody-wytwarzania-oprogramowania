package api.controller;

import api.entity.ExchangeRate;
import api.repository.ExchangeRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3001", maxAge = 3600)

@RestController
@RequestMapping("exchange-rate")
class ExchangeRateController {

    private final ExchangeRateRepository exchangeRateRepository;

    @Autowired
    public ExchangeRateController(ExchangeRateRepository exchangeRateRepository) {
        this.exchangeRateRepository = exchangeRateRepository;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<ExchangeRate> all() {
        return exchangeRateRepository.findAll();
    }

    @RequestMapping(value = "/{bank}", method = RequestMethod.GET)
    public List<ExchangeRate> allForBank(@PathVariable String bank) {
        return exchangeRateRepository.findByBank(bank);
    }

    @RequestMapping(value = "/{bank}/{currency}", method = RequestMethod.GET)
    public List<ExchangeRate> currency(@PathVariable String currency, @PathVariable String bank) {
        return exchangeRateRepository.findByBankAndCurrency(bank, currency);
    }

}


