package api.controller;


import api.config.JWT.UserPrincipal;
import api.entity.*;
import api.repository.ExchangeRateRepository;
import api.repository.UserRepository;
import api.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

@CrossOrigin(origins = "http://localhost:3001", maxAge = 3600)
@RestController
public class UserController {

    private final UserRepository userRepository;
    private final WalletRepository walletRepository;
    private final ExchangeRateRepository exchangeRateRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserRepository userRepository, PasswordEncoder passwordEncoder, WalletRepository walletRepository, ExchangeRateRepository exchangeRateRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder=passwordEncoder;
        this.walletRepository = walletRepository;
        this.exchangeRateRepository = exchangeRateRepository;
    }

    @RequestMapping(value = "registration", method = RequestMethod.POST)
    public String add(@Valid @RequestBody User user) {
        if(userRepository.findByUsername(user.getUsername())!=null)
            return "username zajęty";
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        Wallet wallet = new Wallet();
        wallet.setUserId(user.getId());
        walletRepository.save(wallet);
        return "dodano";
    }

    @RequestMapping(value = "get_wallet", method = RequestMethod.GET)
    public Wallet getWallet(Authentication authentication) {
        UserPrincipal user = (UserPrincipal) authentication.getPrincipal();
        Long userId = user.getId();
        return walletRepository.findByUserId(userId);
    }

    @RequestMapping(value = "reset_password", method = RequestMethod.POST)
    public String resetPassword(@RequestBody ForResetPassword forResetPassword) {
        User user = userRepository.findByUsername(forResetPassword.getUsername());
        System.out.println();
        if (user.getAnswer().equals(forResetPassword.getAnswer()))
        {
            String newPassword = generateRandomString(8);
            userRepository.setPassword(newPassword, user.getUsername());
            return newPassword;
        }
        else {
            return "zla odpowiedz";
        }
    }

    public static String generateRandomString(int length) {

        String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
        String CHAR_UPPER = CHAR_LOWER.toUpperCase();
        String NUMBER = "0123456789";

        String DATA_FOR_RANDOM_STRING = CHAR_LOWER + CHAR_UPPER + NUMBER;
        SecureRandom random = new SecureRandom();

        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int rndCharAt = random.nextInt(DATA_FOR_RANDOM_STRING.length());
            char rndChar = DATA_FOR_RANDOM_STRING.charAt(rndCharAt);
            sb.append(rndChar);

        }

        return sb.toString();

    }

    @CrossOrigin(origins = "http://localhost:3001", maxAge = 3600)
   @RequestMapping(value = "/transfer/{number}", method = RequestMethod.POST)
    public void transfer(@PathVariable Float number, Authentication authentication) {
        UserPrincipal user = (UserPrincipal) authentication.getPrincipal();
        Long userId = user.getId();
        walletRepository.transfer(userId, number);
    }

    @RequestMapping(value = "/exchange", method = RequestMethod.POST)
    public String exchange(@RequestBody InfoForExchange infoForExchange, Authentication authentication) {
        UserPrincipal user = (UserPrincipal) authentication.getPrincipal();
        Long userId = user.getId();
        ExchangeRate exchangeRate = exchangeRateRepository.findByBankAndCurrency(infoForExchange.getBank(), infoForExchange.getCurrency()).get(0);
        Float PLN = walletRepository.numberOfPLN(userId);
        Float rate = exchangeRate.getRate();
        if(PLN/rate<infoForExchange.getNumber())
        {
            return "mało PLN";
        }
        else
        {
            switch(infoForExchange.getCurrency()) {
                case "USD":
                    walletRepository.exchangeUSD(userId, infoForExchange.getNumber(), infoForExchange.getNumber()*rate);
                    break;
                case "EUR":
                    walletRepository.exchangeEUR(userId, infoForExchange.getNumber(), infoForExchange.getNumber()*rate);
                    break;
                case "GBP":
                    walletRepository.exchangeGBP(userId, infoForExchange.getNumber(), infoForExchange.getNumber()*rate);
                    break;
                case "CAD":
                    walletRepository.exchangeCAD(userId, infoForExchange.getNumber(), infoForExchange.getNumber()*rate);
                    break;

            }

            return "wymieniono";
        }
    }
}

