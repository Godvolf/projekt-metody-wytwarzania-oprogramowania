import urllib.request
import time
from datetime import date
import requests


class mBank_Downloader:
	def __init__(self):
		self.url = "https://www.mbank.pl/ajax/currency/getCSV/?id=0&date=[DATE]%20[TIME]&lang=pl"	

	def download(self):
		localtime = time.asctime(time.localtime(time.time()))
		t = str(localtime).split(" ")[4]
		d = str(date.fromtimestamp(time.time()))
		url = self.url
		url = url.replace("[DATE]", d).replace("[TIME]", t)
		urllib.request.urlretrieve(url, './data/mbank.csv')

class pekao_Downloader:
	def __init__(self):
		self.url = "https://www.pekao.com.pl/.currencies/file?type=TEXT&source=PEKAO&date=[DATE]&table=1"	

	def download(self):
		localtime = time.asctime(time.localtime(time.time()))
		d = str(date.fromtimestamp(time.time()))
		url = self.url
		url = url.replace("[DATE]", d)
		urllib.request.urlretrieve(url, './data/pekao.txt')

class nbp_Downloader:
	def __init__(self):
		self.url = "http://api.nbp.pl/api/exchangerates/tables/C/"

	def download(self):
		res = requests.get(self.url)
		data = res.json()[0]
		self.tid = data["no"]
		f = open("./data/nbp.txt","w")
		f.write(self.tid)
		f.close()
		return data