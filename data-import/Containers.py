class Data_Container:
	
	def __init__(self, name, date, rates, tid):
		self.rates = rates
		self.name = name
		self.date = date
		self.tid = tid


	def __str__(self):
		result = ""
		result += "----------------------------------\n"
		result += "Bank name: " + self.name + "\n"
		result += "Date: " + self.date + "\n"
		result += "Exchange rates:\n"
		for key, value in self.rates.items():
			result += key + " : " + str(value) + "\n"
		result += "----------------------------------\n"
		return result


	def export_data(self, cur, i):
		querry = """INSERT INTO exchange_rate (id, bank, currency, date, rate) VALUES (%s, %s, %s, %s, %s);"""
		rates = {"USD": self.rates["USD"], "EUR": self.rates["EUR"], "CAD": self.rates["CAD"], "GBP": self.rates["GBP"]}
		for key, value in rates.items():
			cur.execute(querry, (i, self.name, key, self.date, value[1]))
			i += 1
		return i


