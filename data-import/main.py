import psycopg2
import Parsers
from Containers import Data_Container
import Downloaders
import sys
import time
from Utils import LoadConfig


loader = LoadConfig()
loader.load()

try:
	conn = psycopg2.connect(host=loader.get_host(),database=loader.get_database(), user=loader.get_username(), password=loader.get_password())
	cur = conn.cursor()

	while True:
		try:
			print("[INFO] Loading previous downloaded rates")
			try:
				mbank_last = Parsers.mBank_parser("mbank.csv")
			except FileNotFoundError:
				print("[INFO]     mbank: file not found")
				mbank_last = Data_Container("","",{},"")

			try:
				pekao_last = Parsers.pekao_parser("pekao.txt")
			except FileNotFoundError:
				print("[INFO]     pekao: file not found")
				pekao_last = Data_Container("","",{},"")

			try:
				f = open("./data/nbp.txt","r")
				nbp_last = f.read()
				f.close()
			except FileNotFoundError:
				print("[INFO]     nbp: file not found")
				nbp_last = ""


			print("[INFO] Downloading current rates")
			Downloaders.mBank_Downloader().download()
			Downloaders.pekao_Downloader().download()
			nbp_data = Downloaders.nbp_Downloader().download()

			mbank_new = Parsers.mBank_parser("mbank.csv")
			pekao_new = Parsers.pekao_parser("pekao.txt")
			nbp_new = Parsers.nbp_parser(nbp_data)

			cur.execute("SELECT MAX(id) FROM exchange_rate;")
			i = cur.fetchone()
			i = i[0]
			if i == None:
				i = 0
			else:
				i += 1

			print("[INFO] Comparing differences")
			if mbank_last.tid != mbank_new.tid:
				print("[INFO]     mbank: new data")
				i = mbank_new.get_data().export_data(cur,i)
				conn.commit()
			else:
				print("[INFO]     mbank: no new data found")

			if pekao_last.tid != pekao_new.tid:
				print("[INFO]     pekao: new data")
				i = pekao_new.get_data().export_data(cur,i)
				conn.commit()
			else:
				print("[INFO]     pekao: no new data found")

			if nbp_last != nbp_new.tid:
				print("[INFO]     nbp: new data")
				i = nbp_new.get_data().export_data(cur,i)
				conn.commit()
			else:
				print("[INFO]     nbp: no new data found")

			time.sleep(300)
		except KeyboardInterrupt:
			print("\nExit")
			break
	
	cur.close()
	conn.close()

except psycopg2.OperationalError:
	print("Wrong data in config")


