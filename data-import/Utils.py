class LoadConfig:

	def __init__(self):
		self.file = "config.txt"
		self.username = ""
		self.password = ""
		self.database = ""
		self.host = "localhost"

	def get_host(self):
		return self.host

	def get_password(self):
		return self.password

	def get_username(self):
		return self.username

	def get_database(self):
		return self.database

	def set_config_file(self, filename):
		self.file = filename

	def load(self):
		f = open("./"+self.file,'r')
		content = f.read()
		content = content.split("\n")
		for line in content:
			if line.startswith("database="):
				line = line[9:]
				if line[0] == '"' and line[len(line)-1] == '"':
					line = line[1:]
					line = line[:len(line)-1]
					self.database = line
			elif line.startswith("username="):
				line = line[9:]
				if line[0] == '"' and line[len(line)-1] == '"':
					line = line[1:]
					line = line[:len(line)-1]
					self.username = line
			elif line.startswith("password="):
				line = line[9:]
				if line[0] == '"' and line[len(line)-1] == '"':
					line = line[1:]
					line = line[:len(line)-1]
					self.password = line
			elif line.startswith("host="):
				line = line[9:]
				if line[0] == '"' and line[len(line)-1] == '"':
					line = line[1:]
					line = line[:len(line)-1]
					self.host = line