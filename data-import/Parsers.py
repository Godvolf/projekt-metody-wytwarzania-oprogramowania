from Containers import Data_Container
import csv
import re

class mBank_parser:
	
	def __init__(self, file_name):
		self.name = "mBank"
		self.date = ""
		self.rates = {}
		self.tid = ""

		with open('./data/'+file_name, 'r') as f:
			reader = csv.reader(f)
			data = list(reader)

		data = list(map(lambda x: x[0], data))
		self.tid = data[1]
		self.date = data[1].split(" ")[3]

		data = data[3:]
		data = list(map(lambda x: x.split(";"), data))
		data = list(map(lambda x: {x[1]: (x[4], x[5])}, data))
		self.rates = {}
		for d in data:
			self.rates.update(d)
	
	def get_data(self):
		return Data_Container(name=self.name, date=self.date, rates=self.rates, tid=self.tid)


class pekao_parser:
	def __init__(self, file_name):
		self.name = "PEKAO"
		self.date = ""
		self.rates = {}
		self.tid = ""

		with open('./data/'+file_name, 'r') as f:
			reader = csv.reader(f)
			data = list(reader)

		data = list(map(lambda x: x[0], data))
		self.tid = data[2]
		date = data[2].split(" ")[19]
		self.date = date[6:] + "-" + date[3:5] + "-" + date[0:2]

		data = data[8:-8]
		data = list(map(lambda x: re.split("[ ]+", x), data))

		data[2] = [data[2][0], data[2][1], data[2][2]+data[2][3], data[2][4], data[2][5], data[2][6], data[2][7], data[2][8]]
		data[4] = [data[4][0], data[4][1], data[4][2]+data[4][3], data[4][4], data[4][5], data[4][6], data[4][7], data[4][8]]
		data = list(map(lambda x: {x[7]: (x[4], x[5])}, data))
		self.rates = {}
		for d in data:
			self.rates.update(d)

	def get_data(self):
		return Data_Container(name=self.name, date=self.date, rates=self.rates, tid=self.tid)


class nbp_parser:
	def __init__(self, data):
		self.name = "NBP"
		self.date = ""
		self.rates = {}
		self.tid = ""
		
		self.date = data["effectiveDate"]
		self.tid = data["no"]
		for x in data["rates"]:
			self.rates[x["code"]] = (x["bid"], x["ask"])

	def get_data(self):
		return Data_Container(name=self.name, date=self.date, rates=self.rates, tid=self.tid)