package com.mwo.exchangeapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Response
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.password
import kotlinx.android.synthetic.main.activity_login.username
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        if(loginButton.isClickable())
            Log.d("Login", "clickablde")
        else
            Log.d("Login", "Not - clickablde")
    }

    fun onSignInClick(view : View){
        authenticate(username.text.toString(), password.text.toString())

    }

    private fun authenticate(username : String, password : String) : Boolean {
        val connector = Connector(this)
        val js = JSONObject()
        js.put("username", username)
        js.put("password", password)


        connector.sendJsonRequest(true, "/authenticate", Response.Listener {
            Connector.token = "Bearer "+it.get("token").toString()
            Connector.isLogged = true;
            Toast.makeText(this, "Success", Toast.LENGTH_LONG).show()
            Log.d("TOKEN_LOG", Connector.token )
            Connector.loginText= username
        }, js, Response.ErrorListener {
            Log.d("TOKEN_LOG", it.message )
            Toast.makeText(this, "Failure", Toast.LENGTH_LONG).show()
        })
        return true
    }
}
