package com.mwo.exchangeapp

import android.app.DownloadManager
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.android.volley.Response
import kotlinx.android.synthetic.main.activity_main.*
import com.android.volley.Request
import com.android.volley.Response.Listener
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.fragment_rate.*
import org.json.JSONArray
import org.json.JSONObject
import java.time.format.DateTimeFormatter
import java.util.*

class MainActivity : AppCompatActivity(),
    RegisterLoginFragment.OnFragmentInteractionListener,
    UtilitiesForLoggedFragment.OnFragmentInteractionListener,
    RateFragment.OnFragmentInteractionListener{
    val TAG = "MAIN"
    val CODE_LOGIN_ACTIVITY = 111

    override fun onConfirmClick(view : View) {
        var f = fragment as RateFragment
        Log.d("MAIN", "onConfirmClick")
        refreshView(spinner.selectedItem.toString())
        //f.update()
    }

    public fun updateNow(list : List<Rate>) {
        var f = fragment as RateFragment
        f.update(list)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setTopPanel()
        setupSpiner(R.id.spinner,R.array.currency_array)
    }

    override fun goToWallet(view : View) {
        val intent = Intent(this, WalletActivity::class.java)
        startActivity(intent)
    }

    override fun gotToTransfer(view : View) {
        val intent = Intent(this, TranspherActivity::class.java)
        startActivity(intent)
    }

    override fun goToResetPassword(view : View) {
        val intent = Intent(this, ResetPasswordActivity::class.java)
        startActivity(intent)
    }



    override fun goToExchange(view : View) {
        val intent = Intent(this, ExchangeActivity::class.java)
        startActivity(intent)
    }

    override fun goToLogIn(view : View) {
        Log.d(TAG, "Go to log in")


        val intent = Intent(this, LoginActivity::class.java)
        startActivityForResult(intent,  CODE_LOGIN_ACTIVITY )
    }

    override fun goToRegister(view : View) {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }

    override fun logOut(view : View) {
        Connector.isLogged = false
        setTopPanel()
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "Reume")
        setTopPanel()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode ==  CODE_LOGIN_ACTIVITY ) {
            Log.d(TAG, "Back from login")
            //setTopPanel()
        }
    }

    private fun setTopPanel() {
        if (Connector.isLogged) {
            Log.d(TAG, "Logged")
            supportFragmentManager.beginTransaction().replace(R.id.frameLayout,
                UtilitiesForLoggedFragment.newInstance()).commit()
        } else {
            Log.d(TAG, "not Logged")
            supportFragmentManager.beginTransaction().replace(R.id.frameLayout,
                RegisterLoginFragment.newInstance()).commit()
        }
    }

    inner class rateJsonArrayListener(val curr: String):Listener<JSONArray>{
        override fun onResponse(response: JSONArray?) {
            val list = ArrayList<Rate>()
            val map = HashMap<String, String>()


            for(i in 0 until response!!.length()) {
                val obJson = response.getJSONObject(i)
                 if(obJson.get("currency").toString().compareTo(curr)==0) {
                if (getMaximal(obJson.get("bank").toString(),obJson.get("currency").toString(), response)
                        .compareTo(obJson.get("date").toString())==0){
                    list.add(Rate(obJson.get("bank").toString(),obJson.get("currency").toString(),
                        obJson.get("rate").toString(), obJson.get("rate").toString()))
                }
                }
            }
            updateNow(list)
        }

        public fun getMaximal(bank: String, currency : String,
                              response: JSONArray?) : String {
            var maxi = "01.01.2019"
            for(i in 0 until response!!.length()) {
                val obJson = response.getJSONObject(i)
                if (obJson.get("bank").toString()==bank &&
                    obJson.get("currency").toString() == currency) {
                    if (maxi == "") {
                        maxi = obJson.get("date").toString();
                    } else {
                        if (maxi.compareTo(obJson.get("date").toString()) < 0) {
                            maxi = obJson.get("date").toString();
                        }
                    }
                }
            }
            return maxi;
        }
    }



    private fun setupSpiner(spinerId : Int, arrayId : Int) {

        val spinner: Spinner? = findViewById<Spinner>(spinerId)
        Log.d(TAG, "setup spinner")
        ArrayAdapter.createFromResource(
            this,
            arrayId,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner?.adapter = adapter
            spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    Log.d(TAG, "OnItem selected")
                    Log.d(TAG, resources.getStringArray(arrayId)[position])
                    refreshView(resources.getStringArray(arrayId)[position])
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    //To change body of created functions use File | Settings | File Templates.
                }

            }
        }
    }

    private fun refreshView(currency: String) {
        val conn = Connector(this)
        conn.sendJsonArrayRequest(false, "/exchange-rate/all",
            rateJsonArrayListener(currency))

    }

}