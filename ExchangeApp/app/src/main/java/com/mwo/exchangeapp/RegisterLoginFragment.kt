package com.mwo.exchangeapp

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_register_login.*


class RegisterLoginFragment : Fragment() {
    lateinit var but : Button
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_register_login, container, false)
        but = view.findViewById<Button>(R.id.buttonLogin)
        //but.setOnClickListener { v -> onLogInClick(v) }


        return inflater.inflate(R.layout.fragment_register_login, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }


    fun goToLogIn(view :  View) {
        Log.d("Fragment", "Go to log in")
        listener?.goToLogIn(view)
    }

    fun goToRegister(view :  View) {

        listener?.goToRegister(view)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun goToLogIn(view : View)
        fun goToRegister(view : View )
    }

    companion object {
        @JvmStatic
        fun newInstance() = RegisterLoginFragment()
    }
}
