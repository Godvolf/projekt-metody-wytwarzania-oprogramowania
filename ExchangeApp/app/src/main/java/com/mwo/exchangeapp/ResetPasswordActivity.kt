package com.mwo.exchangeapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Response
import kotlinx.android.synthetic.main.activity_exchange.*
import kotlinx.android.synthetic.main.activity_reset_password.*
import org.json.JSONObject

class ResetPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
    }

    fun onResetClick(view : View) {
        resetPassword(sec_answer.text.toString())

    }

    private fun resetPassword(answer : String) {
        val con = Connector(this)
        val jsonObject = JSONObject()

        jsonObject.put("username", Connector.loginText)
        jsonObject.put("answer", answer)


        Log.d("ResretAccc",jsonObject.toString())
        con.sendJsonReqWithTokenCustom(true,"/reset_password", Response.Listener {  },
            Response.ErrorListener {
                // Log.d("ExchangeAcc", "Piesel"+String(it.networkResponse.data))
                Log.d("ResetAcc", it.message)
                Log.d("ResetAcc", String(it.networkResponse.data))

                Log.d("ResetAcc", " Kak"+it.networkResponse.toString())
                if(it.message!!.contains("zla odpowiedz"))
                    Toast.makeText(this, "wymieniono", Toast.LENGTH_LONG).show()
                else
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()}, jsonObject )
    }
}
