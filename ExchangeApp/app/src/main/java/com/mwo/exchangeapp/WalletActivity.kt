package com.mwo.exchangeapp

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.android.volley.Response
import kotlinx.android.synthetic.main.activity_wallet.*

class WalletActivity : AppCompatActivity() {
    private var listViewAdapter: WalletArrayAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet)
        val con = Connector(this)

        con.sendJsonReqWithToken(false, "/get_wallet", Response.Listener {
            Log.e("Waller","Winner")
            val list =  ArrayList<Pair<String, String>>()
            list.add(Pair(it.get("pln").toString(), "PLN"))
            list.add(Pair(it.get("usd").toString(), "USD"))
            list.add(Pair(it.get("cad").toString(), "CAD"))
            list.add(Pair(it.get("gbp").toString(), "GBP"))
            list.add(Pair(it.get("eur").toString(), "EUR"))
            listViewAdapter = WalletArrayAdapter(this, list)

            walletListView.adapter = listViewAdapter
            listViewAdapter!!.notifyDataSetChanged()
            Log.d("Waller", "Why")

        },Response.ErrorListener { Log.e("Waller",it.message) }, null)

    }

    private class WalletArrayAdapter(context: Context, var data: ArrayList<Pair<String, String>>) :
        ArrayAdapter<Pair<String, String>>(context, R.layout.rate_row, data)  {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var view = convertView
            Log.d("Adapter", "Next item pos:"+position)
            if (view == null) {
                val inflater = LayoutInflater.from(context)
                view = inflater.inflate(R.layout.wallet_row_layout, parent, false)
            }

            view!!.findViewById<TextView>(R.id.textViewWalletCurrency).text = data[position].first
            view!!.findViewById<TextView>(R.id.textViewWallletNumber).text = data[position].second

            return view
        }
    }
}
