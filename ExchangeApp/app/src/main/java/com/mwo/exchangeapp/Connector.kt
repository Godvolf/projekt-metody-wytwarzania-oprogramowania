package com.mwo.exchangeapp

import android.content.Context
import android.util.Log
import com.android.volley.NetworkResponse
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.Response.Listener
import com.android.volley.toolbox.*
import org.json.JSONArray
import org.json.JSONObject
import java.nio.charset.Charset

class Connector(val mContext : Context) {
    private val urlStart = "http://10.42.0.1:4100"
    private val TAG = "Connector"
    private val mQueue = Volley.newRequestQueue(mContext)
    companion object{
        public var token = ""
        var isLogged = false
        public var loginText = ""
    }


    public fun sendJsonReqWithToken(
        isPost: Boolean, urlEnd: String,
        listener: Listener<JSONObject>,
        err: Response.ErrorListener,
        jsObj: JSONObject?
    ) {
        Log.d(TAG, "sendJson BEFORE")
        val reqMethod = when(isPost){
            true -> Request.Method.POST
            false -> Request.Method.GET
        }

        val jsonObjectRequest = object:JsonObjectRequest( reqMethod,
            urlStart + urlEnd, jsObj, listener, err){
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = token
                return headers
            }
        }
        mQueue.add(jsonObjectRequest)
    }


    public fun sendStringRequest(isPost : Boolean, urlEnd : String, listener:
    Listener<String>) {
        val reqMethod = when(isPost){
            true -> Request.Method.POST
            false -> Request.Method.GET
        }
        val stringRequest = object:StringRequest(reqMethod, urlStart+urlEnd, listener,
            Response.ErrorListener { error ->
                // TODO: Handle error
                Log.e(TAG, error.message)
                // Log.e(TAG, String(error.networkResponse.data))
            }){
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = token
                return headers
            }

        }

        Log.d(TAG, "i am");

        mQueue.add(stringRequest)
    }

    public fun sendJsonArrayRequest(isPost : Boolean, urlEnd : String,
                                    listener : Listener<JSONArray>) {
        Log.d(TAG, "sendJson BEFORE")
        val reqMethod = when(isPost){
            true -> Request.Method.POST
            false -> Request.Method.GET
        }

        val jsonArrayRequest = JsonArrayRequest( reqMethod,
            urlStart + urlEnd, null,listener,
            Response.ErrorListener { error ->
                // TODO: Handle error

                // Log.e(TAG, String(error.networkResponse.data))
            }
        )

        mQueue.add(jsonArrayRequest)
        Log.d(TAG, "sendJson AFTER")
    }
    public fun sendJsonRequest(isPost : Boolean, urlEnd : String,
                               listener : Listener<JSONObject>, jsObj: JSONObject, err :Response.ErrorListener) {
        Log.d(TAG, "sendJson BEFORE")
        val reqMethod = when(isPost){
            true -> Request.Method.POST
            false -> Request.Method.GET
        }

        val jsonObjectRequest = JsonObjectRequest( reqMethod,
            urlStart + urlEnd, jsObj, listener, err)

        Log.d(TAG, String(jsonObjectRequest.body))

        mQueue.add(jsonObjectRequest)
    }

    public fun sendJsonReqWithTokenCustom(
        isPost: Boolean, urlEnd: String,
        listener: Listener<JSONObject>,
        err: Response.ErrorListener,
        jsObj: JSONObject?
    ) {
        Log.d(TAG, "sendJson BEFORE")
        val reqMethod = when(isPost){
            true -> Request.Method.POST
            false -> Request.Method.GET
        }


        val jsonObjectRequest = object:jString( reqMethod,
            urlStart + urlEnd, jsObj, listener, err){
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["Authorization"] = token
                return headers
            }
        }
        mQueue.add(jsonObjectRequest)
    }


    open inner class jString(method: Int,
                             url: String?,
                             jsonRequest: JSONObject?,
                             listener: Listener<JSONObject>?,
                             errorListener: Response.ErrorListener?
    ) : JsonObjectRequest(method, url, jsonRequest, listener, errorListener) {

        override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject> {
            Log.d(TAG,"Aniol " + HttpHeaderParser.parseCharset(response!!.headers))
            Log.d(TAG, String(response.data, (Charset.forName(HttpHeaderParser.parseCharset(response!!.headers)))))
            Log.d(TAG, String(response.data))
           // Log.d(TAG, response.data.toString(HttpHeaderParser.parseCharset(response.headers));

            return super.parseNetworkResponse(response)
        }

    }
}