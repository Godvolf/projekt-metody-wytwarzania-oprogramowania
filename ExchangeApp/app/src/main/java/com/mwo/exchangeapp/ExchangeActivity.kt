package com.mwo.exchangeapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.android.volley.Response
import kotlinx.android.synthetic.main.activity_exchange.*
import org.json.JSONObject

class ExchangeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exchange)
        setupSpiner(R.id.spinnerBank, R.array.bank_array)
        setupSpiner(R.id.spinnerBankCurrency, R.array.currency_array)

    }

    private fun setupSpiner(spinerId : Int, arrayId : Int) {
        val spinner: Spinner? = findViewById<Spinner>(spinerId)
        ArrayAdapter.createFromResource(
            this,
            arrayId,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner?.adapter = adapter
            spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    //To change body of created functions use File | Settings | File Templates.
                }

            }
        }
    }


    fun onExchangeClick(view : View) {
        val con = Connector(this)
        val jsonObject = JSONObject()
        Log.d("ExchangeAcc", spinnerBank.selectedItem.toString())
        Log.d("ExchangeAcc", spinnerBankCurrency.selectedItem.toString().toLowerCase())
        Log.d("ExchangeAcc", editTextYourMoney.text.toString())
        jsonObject.put("bank", spinnerBank.selectedItem.toString())
        jsonObject.put("currency", spinnerBankCurrency.selectedItem.toString())
        jsonObject.put("number", editTextYourMoney.text.toString().toFloat())
        Log.d("ExchangeAcc",jsonObject.toString())
        con.sendJsonReqWithToken(true,"/exchange", Response.Listener {  },
            Response.ErrorListener {
               // Log.d("ExchangeAcc", "Piesel"+String(it.networkResponse.data))
                if(it.message!!.contains("wymieniono"))
                Toast.makeText(this, "wymieniono", Toast.LENGTH_LONG).show()
            else
                Toast.makeText(this, "za mało pieniedzy", Toast.LENGTH_LONG).show()}, jsonObject )

    }
}
