package com.mwo.exchangeapp

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

class RateArrayAdapter(context: Context, var data: ArrayList<Rate>) :
    ArrayAdapter<Rate>(context, R.layout.rate_row, data)  {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        Log.d("Adapter", "Next item pos:"+position)
        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.rate_row, parent, false)
        }

        view!!.findViewById<TextView>(R.id.textViewCurrency).text = data[position].currencry
        view!!.findViewById<TextView>(R.id.textViewBankName).text = data[position].bankName
        view!!.findViewById<TextView>(R.id.textViewBuyPrice).text = data[position].buyPrice

        return view
    }
}