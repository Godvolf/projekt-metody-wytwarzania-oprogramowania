package com.mwo.exchangeapp

data class Rate(val bankName : String, val currencry : String,
        val buyPrice : String, val data : String) {
}