package com.mwo.exchangeapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Response
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject

class RegisterActivity : AppCompatActivity() {
    val mAdded = "dodano"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }

    fun onRegisterClick(view : View) {
        performRegistration(username.text.toString(),
            password.text.toString(), first_name.text.toString(),
            sec_question.text.toString(), sec_answer.text.toString(),
            last_name.text.toString())


    }

    fun performRegistration(usernam : String, password : String, firstName : String,
                            quest : String, ans: String, last: String){
        val connector = Connector(this)
        val js = JSONObject()
        js.put("firstName", firstName)
        js.put("lastName", last)
        js.put("username", usernam)
        js.put("password", password)
        js.put("securityQuestion", quest)
        js.put("answer", ans)

        connector.sendJsonRequest(true, "/registration", Response.Listener {
            Log.d("Alaa", it.toString())
        }, js, Response.ErrorListener {
            Log.d("Alaa", it.message)
            if(it.message!!.contains("dodano"))
                Toast.makeText(this, "dodano", Toast.LENGTH_LONG).show()
            else
                Toast.makeText(this, "zajęty", Toast.LENGTH_LONG).show()
        })

    }
}
