package com.mwo.exchangeapp

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_utilities_for_logged.*


class UtilitiesForLoggedFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_utilities_for_logged, container, false)
    }

    override fun onStart(){
        super.onStart()
        textViewLogin.text = Connector.loginText
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
    fun logOut(view : View){

    }
    fun goToExchange(view : View){

    }
    fun goToWallet(view : View){

    }
    fun gotToTransfer(view :View){

    }

    fun goToResetPassword(view : View){

    }

    interface OnFragmentInteractionListener {
        fun logOut(view : View)
        fun goToExchange(view : View)
        fun goToWallet(view : View)
        fun gotToTransfer(view :View)
        fun goToResetPassword(view : View)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
        */
        @JvmStatic
        fun newInstance() = UtilitiesForLoggedFragment()
    }
}
