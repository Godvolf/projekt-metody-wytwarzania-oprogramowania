package com.mwo.exchangeapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.android.volley.Response
import kotlinx.android.synthetic.main.activity_transpher.*

class TranspherActivity : AppCompatActivity() {
    val mRegex = Regex("    [0-9]*\\.[0-9][0-9]")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transpher)
    }

    fun onTranspherClick(view : View) {
        val text =editTextMoney.text.toString();
        transpherMoney(text)
       /*if (mRegex.matches(text)) {
            transpherMoney(text)
        }*/
    }

    private fun setupSpiner(spinerId : Int, arrayId : Int) {
        val spinner: Spinner? = findViewById<Spinner>(spinerId)
        ArrayAdapter.createFromResource(
            this,
            arrayId,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner?.adapter = adapter
            spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    //To change body of created functions use File | Settings | File Templates.
                }

            }
        }
    }

    fun transpherMoney(num : String) {
        val connector = Connector(this)
        connector.sendStringRequest(true, "/transfer/$num", Response.Listener {
            Log.d("Transfer", "OK" )
            Toast.makeText(this, "Succesful Transfer", Toast.LENGTH_LONG).show()
        })
    }
}
