package com.mwo.exchangeapp

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.fragment_rate.*


class RateFragment : Fragment() {
    private var listView: ListView? = null
    private var listener: OnFragmentInteractionListener? = null
    private var listViewAdapter: RateArrayAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_rate, container, false)
        if (listViewRates == null)
        {
            Log.d("Rate", "Easy null")
        }
        listViewAdapter = context?.let { RateArrayAdapter(it, ArrayList<Rate>()) }

        return view
    }

    override fun onStart(){
        super.onStart()
        listViewRates.adapter = listViewAdapter
        val spinner: Spinner? = view?.findViewById<Spinner>(R.id.spinner)
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            context,
            R.array.currency_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner?.adapter = adapter
            spinner?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    //To change body of created functions use File | Settings | File Templates.
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    //To change body of created functions use File | Settings | File Templates.
                }

            }
        }

    }




    fun onConfirmClick(view : View) : Boolean {
        Log.d("Rate", "confirm click")
        //listener?.onParamsConfirm()
        return true
    }
    interface OnFragmentInteractionListener {
        fun onConfirmClick(view : View)
    }
    public fun update(list : List<Rate>) {
        Log.d("Rate", "update")
        if (listViewAdapter == null) {
            Log.d("Rate", "nullek")
        } else {
            Log.d("Rate", "not-nullek")
        }
        listViewAdapter = context?.let { RateArrayAdapter(it, list as ArrayList<Rate>) }
        listViewRates.adapter = listViewAdapter
        //listViewRates.adapter  = listViewAdapter
        listViewAdapter?.notifyDataSetChanged()
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) = RateFragment()
    }
}
