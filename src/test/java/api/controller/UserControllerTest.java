package api.controller;

import api.entity.Wallet;
import api.repository.UserRepository;
import api.repository.WalletRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.hamcrest.Matchers.equalTo;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ComponentScan(basePackages = {"api.controller"})
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void getWallet() throws Exception{
        mockMvc.perform(get("/get_wallet"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void resetPassword() throws Exception{

        mockMvc.perform(get("/get_wallet"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void transfer() throws Exception{

        mockMvc.perform(get("/get_wallet"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void exchange() throws Exception{

        mockMvc.perform(get("/get_wallet"))
                .andExpect(status().isUnauthorized());
    }
}
