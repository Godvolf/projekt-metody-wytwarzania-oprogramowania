package api.controller;

import api.entity.ExchangeRate;
import api.entity.Wallet;
import api.repository.ExchangeRateRepository;
import api.repository.UserRepository;
import api.repository.WalletRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ComponentScan(basePackages = {"api.controller"})
@SpringBootTest
@AutoConfigureMockMvc
public class ExchangeRateControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ExchangeRateRepository exchangeRateRepository;

    @Test
    public void all() throws Exception{

         when(exchangeRateRepository.findAll()).thenReturn(Arrays.asList(
                 new ExchangeRate(LocalDate.now(), "PEKAO", "USD", 3.82f),
                 new ExchangeRate(LocalDate.now(), "mBank", "EUR", 4.00f)
         )

        );

        mockMvc.perform(get("/exchange-rate/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].bank", containsInAnyOrder("PEKAO","mBank")))
                .andExpect(jsonPath("$[*].currency", containsInAnyOrder("USD","EUR")));
    }

    @Test
    public void allForBank() throws Exception{
        when(exchangeRateRepository.findByBank(anyString())).thenReturn(Arrays.asList(
                new ExchangeRate(LocalDate.now(), "PEKAO", "USD", 3.82f),
                new ExchangeRate(LocalDate.now(), "PEKAO", "EUR", 4.00f)
                )

        );

        mockMvc.perform(get("/exchange-rate/PEKAO"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].bank", containsInAnyOrder("PEKAO", "PEKAO")))
                .andExpect(jsonPath("$[*].currency", containsInAnyOrder("USD","EUR")));
    }

    @Test
    public void currency() throws Exception{
        when(exchangeRateRepository.findByBankAndCurrency(anyString(), anyString())).thenReturn(Arrays.asList(
                new ExchangeRate(LocalDate.now(), "PEKAO", "USD", 3.82f)
                )

        );

        mockMvc.perform(get("/exchange-rate/PEKAO/USD"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[*].bank", containsInAnyOrder("PEKAO")))
                .andExpect(jsonPath("$[*].currency", containsInAnyOrder("USD")));
    }
}
